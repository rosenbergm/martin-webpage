#!/bin/bash

echo "Pulling the newest version from Github"
git pull

echo "Installing dependencies"
npm install

echo "Building application"
npm run build

echo "Copying files to NGINX"
cp -R ./build/* /var/www/html

echo "DONE!"