import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import '../css/contacts.css';
import FormComponent from './contacts/form.component';

export default class ContactsComponent extends Component {
  render() {
    return (
      <div className="container-fluid" style={{ margin: 0, padding: 0 }}>
        <div className="container-fluid whiteonblack h-100">
          <div className="wob-container">
            <Link to="/">
              <h6 style={{ fontSize: '1rem', color: '#fff' }}>
                « Back to homepage
              </h6>
            </Link>
            <h1
              style={{
                textTransform: 'uppercase',
                letterSpacing: 8,
                marginBottom: '2rem',
                fontSize: '2.5rem'
              }}
            >
              contact me
            </h1>
            <div className="row justify-content-center">
              <FormComponent />
              <div
                className="col-md-6 no-float socialIcon contactEmail text-center emailContainer"
                style={{ width: '100%', paddingBottom: '4rem' }}
              >
                <h5>Get in touch at</h5>
                <a
                  href="mailto:hello@martinrosenberg.eu"
                  style={{ margin: 0, padding: 0, textDecoration: 'none' }}
                >
                  <h2 style={{ marginBottom: '1rem' }}>
                    hello@martinrosenberg.eu
                  </h2>
                </a>
                <h5 style={{ marginBottom: '1rem' }}>or</h5>
                <a
                  href="https://www.linkedin.com/in/martin-rosenberg-00535187/"
                  target="_blank"
                >
                  <i class="fab fa-linkedin" style={{ fontSize: '250%' }} />
                </a>
                <a href="https://github.com/rosenbergm/" target="_blank">
                  <i
                    class="fab fa-github-square"
                    style={{ fontSize: '250%' }}
                  />
                </a>
                <a
                  href="https://www.facebook.com/martin.rosenberg.980"
                  target="_blank"
                >
                  <i
                    class="fab fa-facebook-square"
                    style={{ fontSize: '250%' }}
                  />
                </a>
                <a href="mailto:hello@martinrosenberg.eu">
                  <i
                    class="fas fa-envelope-square"
                    style={{ fontSize: '250%' }}
                  />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
