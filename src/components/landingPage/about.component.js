import React, { Component } from 'react';

import { Row, Col } from 'reactstrap';

import data from '../data/about.json';

export default class AboutComponent extends Component {
  render() {
    return (
      <div className="container-fluid whiteonblack">
        <div style={{ padding: '5rem' }}>
          <h1
            style={{
              textTransform: 'uppercase',
              letterSpacing: 8,
              marginBottom: '3rem'
            }}
          >
            {this.props.title}
          </h1>
          <Row>
            <Col md={9}>
              <p style={{ fontSize: '130%' }}>{data.about.text}</p>
            </Col>
            <Col md={3}>
              <img
                src={this.props.profile}
                alt="This is me!"
                class="float-right img-fluid rounded"
                style={{ height: 'auto' }}
              />
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}
