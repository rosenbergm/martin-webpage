import React, { Component } from 'react';

import data from '../data/tools.json';

export default class ToolsComponent extends Component {
  render() {
    return (
      <div className="container-fluid whiteonblack">
        <div style={{ padding: '5rem' }}>
          <h1
            style={{
              textTransform: 'uppercase',
              letterSpacing: 3,
              marginBottom: '3rem'
            }}
          >
            {this.props.title}
          </h1>
          <p>{data.tools.text}</p>
        </div>
      </div>
    );
  }
}
