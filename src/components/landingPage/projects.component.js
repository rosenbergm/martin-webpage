import React, { Component } from 'react';

import data from '../data/projects.json';

export default class ProjectsComponent extends Component {
  render() {
    return (
      <div className="container-fluid blackonwhite">
        <div style={{ padding: '5rem' }}>
          <h1
            style={{
              textTransform: 'uppercase',
              letterSpacing: 3,
              marginBottom: '3rem'
            }}
          >
            {this.props.title}
          </h1>

          <div class="table-responsive-md">
            <table class="table table-light table-striped table-bordered">
              <thead>
                <tr>
                  <th scope="col">Name</th>
                  <th scope="col">Description</th>
                  <th scope="col">Language</th>
                  <th scope="col">Date</th>
                  <th scope="col">Link</th>
                </tr>
              </thead>
              <tbody>
                {data.map(row => (
                  <tr>
                    <td>{row.name}</td>
                    <td>{row.description}</td>
                    <td>{row.language}</td>
                    <td>{row.date}</td>
                    <td>
                      {row.link.frontend !== '---' && (
                        <span>
                          <a href={row.link.frontend}>Frontend</a>
                          {', '}
                        </span>
                      )}
                      {row.link.backend !== '---' ? (
                        <a href={row.link.backend}>Backend</a>
                      ) : (
                        '---'
                      )}
                    </td>
                  </tr>
                ))}

                {/* <tr>
                  <td>Tipapp (Neymar)</td>
                  <td>
                    My first actually usable project which allowed a group of
                    people to bet on different sports and matches
                  </td>
                  <td>JavaScript (React, React Native), NodeJS (REST API)</td>
                  <td>2-7/2018</td>
                  <td>---</td>
                </tr>
                <tr>
                  <td>Minicoders</td>
                  <td>
                    Web app created for our local CoderDojo I mentor in. It is
                    used to teach the basics of JavaScript. This was the first
                    time I used GraphQL (and I used it wrong...)
                  </td>
                  <td>JavaScript (React), NodeJS (GraphQL API)</td>
                  <td>8-9/2018</td>
                  <td>
                    <a href="https://github.com/rosenbergm/minicoders-api">
                      Backend
                    </a>
                    ,{' '}
                    <a href="https://github.com/rosenbergm/minicoders-react">
                      Frontend
                    </a>
                  </td>
                </tr>
                <tr>
                  <td>GASP</td>
                  <td>
                    A web hub where you can add and manage your smart home
                    devices from one place. Not only I remade the whole front
                    end I then designed a whole new, fresh modern UI and applied
                    it to the new app.
                  </td>
                  <td>JavaScript (React)</td>
                  <td>1-3/2019</td>
                  <td>---</td>
                </tr>
                <tr>
                  <td>Personal website/portfolio</td>
                  <td>
                    My personal website constructed with React for the most
                    performance. It's connected with a database to create
                    dynamic elements on the pages.
                  </td>
                  <td>JavaScript (React), NodeJS (REST API)</td>
                  <td>1-3/2019</td>
                  <td>This is it, take a look</td>
                </tr> */}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
