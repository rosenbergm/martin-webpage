import React, { Component } from 'react';

import { Button, Form, FormGroup, Label, Input, Row, Col } from 'reactstrap';

export default class FormComponent extends Component {
  render() {
    return (
      <div className="col-md-6 no-float">
        <Form className="whiteText">
          <Row form>
            <Col md={5}>
              <FormGroup>
                <Label for="Name">Name</Label>
                <Input
                  type="text"
                  name="Name"
                  id="Name"
                  placeholder="Your Name"
                />
              </FormGroup>
            </Col>
            <Col md={7}>
              <FormGroup>
                <Label for="Your Email">Email</Label>
                <Input
                  type="email"
                  name="email"
                  id="email"
                  placeholder="Your Email address"
                />
              </FormGroup>
            </Col>
          </Row>
          <FormGroup>
            <Label for="Subject">Subject</Label>
            <Input
              type="text"
              name="subject"
              id="subject"
              placeholder="What is it about?"
            />
          </FormGroup>
          <FormGroup>
            <Label for="Message">Message</Label>
            <Input
              rows="9"
              type="textarea"
              name="message"
              id="message"
              placeholder="Hi Martin,"
            />
          </FormGroup>
          <FormGroup>
            <Button
              style={{
                width: '100%'
              }}
            >
              Submit
            </Button>
          </FormGroup>
        </Form>
      </div>
    );
  }
}
