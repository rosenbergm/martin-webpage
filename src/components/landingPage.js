import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Row, Col } from 'reactstrap';

import landingPageStyles from '../css/landingPage.css';
import me from '../martin-square.webp';
import AboutComponent from './landingPage/about.component';
import ProjectsComponent from './landingPage/projects.component';
import ToolsComponent from './landingPage/tools.component';

export default class LandingPageComponent extends Component {
  render() {
    return (
      <div className="container-fluid" style={{ margin: 0, padding: 0 }}>
        <div className="main">
          <header class="header-bg">
            <nav class="navbar navbar-expand-md sticky-top navbar-dark">
              <a class="navbar-brand" href="/">
                Martin Rosenberg
              </a>
              <button
                class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span class="navbar-toggler-icon" />
              </button>

              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav ml-auto">
                  <li class="nav-item active">
                    <a class="nav-link" href="/contacts">
                      Contact
                    </a>
                  </li>
                </ul>
              </div>
            </nav>
          </header>

          <div className="centerText">
            <div>
              <h1
                style={{
                  textTransform: 'uppercase',
                  fontWeight: 'bold',
                  letterSpacing: 9,
                  fontSize: '2.7rem'
                }}
              >
                Martin Rosenberg
              </h1>
              <h4>STUDENT | DEVELOPER | ELECTRONICS ENTHUSIAST</h4>
            </div>
          </div>
        </div>
        <div className="centerTextMobile">
          <div>
            <h1
              style={{
                textTransform: 'uppercase',
                fontWeight: 'bold',
                letterSpacing: 9
              }}
            >
              Martin Rosenberg
            </h1>
            <h4>
              STUDENT | <br />
              DEVELOPER | <br />
              ELECTRONICS ENTHUSIAST
            </h4>
          </div>
        </div>
        <AboutComponent profile={me} title="About Me" />
        <ProjectsComponent title="My Projects" />
        <ToolsComponent title="Tools and Technologies" />
        <footer class="page-footer">
          <div class="container-fluid text-center text-md-left">
            <div class="row">
              <div class="col-md-6 mt-md-0 mt-3">
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}
                  className={landingPageStyles.socialIcon}
                >
                  <a
                    href="https://www.linkedin.com/in/martin-rosenberg-00535187/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <i class="fab fa-linkedin" style={{ fontSize: '250%' }} />
                  </a>
                  <a
                    href="https://github.com/rosenbergm/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <i
                      class="fab fa-github-square"
                      style={{ fontSize: '250%' }}
                    />
                  </a>
                  <a
                    href="https://www.facebook.com/martin.rosenberg.980"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <i
                      class="fab fa-facebook-square"
                      style={{ fontSize: '250%' }}
                    />
                  </a>
                  <Link to="/contacts/">
                    <i
                      class="fas fa-envelope-square"
                      style={{ fontSize: '250%' }}
                    />
                  </Link>
                </div>
              </div>
              <hr class="clearfix w-100 d-md-none pb-3" />
              <div class="col-md-6 mb-md-0 mb-3">
                <div
                  class="row"
                  className={landingPageStyles.bottomBar}
                  style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    textAlign: 'center',
                    marginTop: '1.5rem'
                  }}
                >
                  <Link
                    to="/contacts/"
                    className="col"
                    style={{ color: '#000', padding: '20px' }}
                  >
                    Contact
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div class="footer-copyright text-center py-3">
            © 2019 Copyright <a href="/">Martin Rosenberg</a>
          </div>
        </footer>
      </div>
    );
  }
}
