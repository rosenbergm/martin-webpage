import React, { Component } from 'react';

import { BrowserRouter as Router, Route } from 'react-router-dom';

import LandingPageComponent from './components/landingPage';
import ContactsComponent from './components/contacts';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route path="/" exact component={LandingPageComponent} />
          <Route path="/contacts" component={ContactsComponent} />
        </div>
      </Router>
    );
  }
}

export default App;
